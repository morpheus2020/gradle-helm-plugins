package com.github.morpheus2020;

import com.google.cloud.artifactregistry.auth.CredentialProvider;
import com.google.cloud.artifactregistry.auth.DefaultCredentialProvider;
import org.gradle.api.Plugin;
import org.gradle.api.Project;

public class HelmPublishPlugin implements Plugin<Project> {
    private CredentialProvider credentialProvider;

    public HelmPublishPlugin() {
        credentialProvider = new DefaultCredentialProvider();
    }

    @Override
    public void apply(Project project) {
        HelmPublishExtension helmPublish = project.getExtensions().create("helmPublish", HelmPublishExtension.class);
        project.getTasks().create("helmPackage", HelmPackageTask.class);
    }
}
