package com.github.morpheus2020;

import org.apache.commons.lang3.ArrayUtils;
import org.gradle.api.DefaultTask;
import org.gradle.api.Project;
import org.gradle.api.artifacts.PublishArtifact;
import org.gradle.api.tasks.TaskAction;
import org.gradle.api.tasks.TaskDependency;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class HelmPackageTask extends DefaultTask {

    public HelmPackageTask() {
        getOutputs().cacheIf(task -> true);
    }

    @TaskAction
    public void execute() {
        Project project = getProject();
        HelmPublishExtension extension = project.getExtensions().getByType(HelmPublishExtension.class);
//        String[] chartDirs = extension.getChartDirs();
//        if (ArrayUtils.isEmpty(chartDirs)) {
//            getLogger().info("The chartDirs are empty, skipping the helmPackage Task");
//        }
        File projectDir = project.getProjectDir();
        File helmDir = new File(projectDir, "src/main/helm");
        if (!helmDir.exists()) {
            getLogger().info("There is no helm chart at " + helmDir.getAbsolutePath());
            return;
        }
        File buildDir = project.getBuildDir();
        File helmOut = new File(buildDir, "helm");
        helmOut.mkdirs();
        ProcessBuilder pb = new ProcessBuilder();
        pb.command("helm", "package", helmDir.getAbsolutePath(), "--destination", helmOut.getAbsolutePath());
        pb.inheritIO();
        try {
            Process process = pb.start();
            int i = process.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
