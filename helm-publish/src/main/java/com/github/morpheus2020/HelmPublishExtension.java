package com.github.morpheus2020;

public class HelmPublishExtension {
    private String bucketName;
    private String[] chartDirs;

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String[] getChartDirs() {
        return chartDirs;
    }

    public void setChartDirs(String[] chartDirs) {
        this.chartDirs = chartDirs;
    }
}
